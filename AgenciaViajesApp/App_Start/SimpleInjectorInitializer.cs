[assembly: WebActivator.PostApplicationStartMethod(typeof(AgenciaViajesApp.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace AgenciaViajesApp.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;
    using AgenciaViajesApp.Services;
    using AgenciaViajesApp.Services.ServicesInterfaces;
    using SimpleInjector;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {

            container.Register<IViajeroService, ViajeroService>(Lifestyle.Scoped);

            // For instance:
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);
        }
    }
}