﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AgenciaViajesApp.Models;
using AgenciaViajesApp.Services;

namespace AgenciaViajesApp.Controllers
{
    public class ViajesController : Controller
    {
        ViajesService service = new ViajesService();

        // GET: Viajes
        public async Task<ActionResult> Index()
        {
            var listaviajes = await service.ListaViajesAsync();
            return View(listaviajes);
        }



        //GET: Viajes/Create
        public ActionResult Create()
        {
            ViewBag.codigo = Guid.NewGuid();
            return View();
        }


        //POST: Viajes/Create
        //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Codigo,Plazas,Origen,Destino,Precio")] Viajes viajes)
        {
            if (ModelState.IsValid)
            {
                bool res = await service.CrearViajesAsync(viajes);
                if (res)
                {
                    return RedirectToAction("Index");
                }                
            }
            return View(viajes);
        }

        // GET: Viajes/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = await service.GetViajeAsync(id);
            return View(model);
        }

        // POST: Viajes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Codigo,Plazas,Origen,Destino,Precio")] Viajes viajes)
        {
            if (ModelState.IsValid)
            {
                bool result = await service.UpdateViajeAsync(viajes);

                if (result)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(viajes);
        }



        // GET: Viajes/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0 )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = await service.GetViajeAsync(id);
            return View(model);
        }



        // POST: Viajes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            bool result = await service.DeleteViajeAsync(id);
            if (result)
            {
                return RedirectToAction("Index");
            }
            return View();         
        }



    }
}
