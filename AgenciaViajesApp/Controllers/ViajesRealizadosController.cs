﻿using AgenciaViajesApp.Models;
using AgenciaViajesApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AgenciaViajesApp.Controllers
{
    public class ViajesRealizadosController : Controller
    {
        ViajeroService serviceViajero = new ViajeroService();
        ViajesService serviceViajes = new ViajesService();
        ViajesRealizadosService service = new ViajesRealizadosService();

    // GET: ViajesRealizados
    public async Task<ActionResult> Index()
        {
            var listaViajesRealizados = await service.ListaViajesRealizadosAsync();

            return View(listaViajesRealizados);
        }
             

        // GET: ViajesRealizados/Create
        public async Task<ActionResult> Create()
        {

            var listaViajeros = await serviceViajero.ListaViajerosAsync();
            var listaViajes = await serviceViajes.ListaViajesAsync();

            
            ViewBag.listViajeros = listaViajeros.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Nombre +" - " + c.Cedula   
            }).ToList();

            ViewBag.listViajes = listaViajes.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Origen + " - " + c.Destino
            }).ToList();

            

            return View();
        }

        // POST: ViajesRealizados/Create
        [HttpPost]
        public async Task<ActionResult> Create(ViajesRealizados viajesRealizados)
        {
            if (ModelState.IsValid)
            {
                bool result = await service.CrearViajeRealizadoAsync(viajesRealizados);
                if (result)
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

      

        // POST: ViajesRealizados/Edit/5
      

        // GET: ViajesRealizados/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = await service.GetViajeRelizadoPorIdAsync(id);

            return View(model);
        }

        // POST: ViajesRealizados/Delete/5     
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            bool result = await service.DeleteViajeRealizadoAsync(id);
            if (result)
            {
                return RedirectToAction("Index");
            }
            return View();

        }

        public async Task<JsonResult> GetViajerosAsyncFromService(int id)
        {
              var viajero = await serviceViajero.GetViajeroAsync(id);
              return Json(new { data = viajero }, JsonRequestBehavior.AllowGet);
        }

      

       public async Task<JsonResult> GetViajesAsyncFromService(int id)
        {
            var viajes = await serviceViajes.GetViajeAsync(id);
            return Json(new { data = viajes }, JsonRequestBehavior.AllowGet);
        }
    }
}
