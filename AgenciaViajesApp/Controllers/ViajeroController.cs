﻿using AgenciaViajesApp.Models;
using AgenciaViajesApp.Services;
using AgenciaViajesApp.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AgenciaViajesApp.Controllers
{
    public class ViajeroController : Controller
    {

        private readonly IViajeroService service;

        public ViajeroController(IViajeroService service)
        {
            this.service = service;
        }
       

        // GET: Viajero
        public async Task<ActionResult> Index()
        {
            var aux = await service.ListaViajerosAsync();
            return View(aux);
        }


        // GET: Viajero/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Viajero/Create
        [HttpPost]
        public async Task<ActionResult> Create(Viajeros viajero)
        {
            if (ModelState.IsValid)
            {               
                viajero.Nombre = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(viajero.Nombre);   
                bool result = await service.CrearViajeroAsync(viajero);
                if (result)
                {
                    return RedirectToAction("Index");
                }              
            }
            return View();
        }


        // GET: Viajero/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = await service.GetViajeroAsync(id);
            return View(model);
        }

        // POST: Viajero/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(Viajeros viajero)
        {
            if (ModelState.IsValid)
            {
                viajero.Nombre = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(viajero.Nombre);
                bool result = await service.UpdateViajeroAsync(viajero);

                if (result)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(viajero);
        }

        // GET: Viajero/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = await service.GetViajeroAsync(id);
            return View(model);
        }

       
        // POST: Viajes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            bool result = await service.DeleteViajeroAsync(id);
            if (result)
            {
                return RedirectToAction("Index");
            }
            return View();
        }



        //////////////////   Metodo edicion viajero con vista parcial /////////////////////////

        // GET: Viajero/Edit/5
        [HttpGet]
        public async Task<PartialViewResult> EditPartial(int id)
        {           
            var model = await service.GetViajeroAsync(id);
            return PartialView(model);
        }

        [HttpPost]
        public async Task<JsonResult> EditPartial(Viajeros viajero)
        {
            if (ModelState.IsValid)
            {
                viajero.Nombre = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(viajero.Nombre);
                bool result = await service.UpdateViajeroAsync(viajero);

                var updatedModel = await service.GetViajeroAsync(viajero.Id);

                //if (result)
                //{
                //    return RedirectToAction("Index");
                //}
                return Json(new { data = updatedModel }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { data = string.Empty }, JsonRequestBehavior.AllowGet);
        }
    }
}
