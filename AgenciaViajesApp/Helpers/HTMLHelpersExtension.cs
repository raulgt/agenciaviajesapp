﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgenciaViajesApp.Helpers
{
    public static class HTMLHelpersExtension
    {
        public static string Truncar (this HtmlHelper helper, string valor, int numeroCaracteresMostrar)
        {
            return (valor.Length <= numeroCaracteresMostrar) ? valor : valor.Substring(0, numeroCaracteresMostrar) + "...";
        }
    }
}