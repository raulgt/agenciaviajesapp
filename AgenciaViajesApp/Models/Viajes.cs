﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AgenciaViajesApp.Models
{
    public class Viajes
    {
   
        public int Id { get; set; }

        [Required]
        public string Codigo { get; set; }

        [Required]
        public int Plazas { get; set; }

        [Required]
        [StringLength(100)]
        public string Origen { get; set; }

        [Required]
        [StringLength(100)]
        public string Destino { get; set; }

        [Required]
        public double Precio { get; set; }
    }
}