﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AgenciaViajesApp.Models
{
    public class Viajeros
    {
        public int Id { get; set; }

        [Required]
        [StringLength(15)]
        public string Cedula { get; set; }

        [Required]
        [StringLength(300)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(25)]
        public string Telefono { get; set; }

    
    }
}