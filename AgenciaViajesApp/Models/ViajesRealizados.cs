﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgenciaViajesApp.Models
{
    public class ViajesRealizados
    {
        public int Id { get; set; }

        public int ViajeroId { get; set; }  

        public int ViajeId { get; set; }

        public DateTime FechaRegistro { get; set; }
       
    }
}