﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgenciaViajesApp.Models
{
    public class ViajesCompletos
    {

        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Cedula { get; set; }

        public string Telefono { get; set; }

        public string CodigoViaje { get; set; }

        public string Origen { get; set; }

        public string Destino { get; set; }

        public double Precio { get; set; }
    }
}