﻿
import * as test from './prueba.js';

$(document).ready(function () {


    var model = {

        urlViajero: '/ViajesRealizados/GetViajerosAsyncFromService',
        urlViaje: '/ViajesRealizados/GetViajesAsyncFromService',
        currentViajero: {
            nombre: '',
            telefono: '',
            cedula: '',
            id: 0
        },
        currentViaje: {
            codigo: '',
            plazas: 0,
            origen: '',
            destino: '',
            precio: 0,
            id: 0
        }
    };

    var controller = {
        init: function () {

            viajeroView.init();
            viajesView.init();

        },
        LoadCurrectViajero: function (viajeroId) {

            test.doAjax(viajeroId, model.urlViajero, 'json').then((res) => {

                if (res.data !== null) {
                    var viajero = { 'id': res.data['Id'], 'telefono': res.data['Telefono'], 'cedula': res.data['Cedula'], 'nombre': res.data['Nombre'] };
                    controller.SetCurrentViajero(viajero);
                    viajeroView.render();
                } else {
                    toastr.error("Ha ocurrido un error al seleccionar el viajero", 'Error', { positionClass: 'toast-bottom-right' });
                }
            }).fail(function (res) {
                toastr.error("Ha ocurrido un error con el servicio", 'Error', { positionClass: 'toast-bottom-right' });
            });


            //var request = $.ajax({
            //    url: model.urlViajero,
            //    method: "GET",
            //    data: { id: viajeroId },
            //    dataType: "json"

            //});

            //request.then(function (res) {
            //    if (res.data !== null) {
            //        var viajero = { 'id': res.data['Id'], 'telefono': res.data['Telefono'], 'cedula': res.data['Cedula'], 'nombre': res.data['Nombre'] };
            //        controller.SetCurrentViajero(viajero);
            //        viajeroView.render();
            //    } else {
            //        toastr.error("Ha ocurrido un error al seleccionar el viajero", 'Error', { positionClass: 'toast-bottom-right' });
            //    }
            //});

            //request.fail(function (res) {
            //    toastr.error("Ha ocurrido un error con el servicio", 'Error', { positionClass: 'toast-bottom-right' });
            //});
        },

        LoadCurrentViaje: async function (viajeId) {

            try {
                var request = $.ajax({
                    url: model.urlViaje,
                    method: "GET",
                    data: { id: viajeId },
                    dataType: "json"
                });

                request.then(function (res) {
                    if (res.data !== null) {
                        var viaje = { 'id': res.data['Id'], 'codigo': res.data['Codigo'], 'plazas': res.data['Plazas'], 'origen': res.data['Origen'], 'destino': res.data['Destino'], 'precio': res.data['Precio'] };
                        controller.SetCurrentViaje(viaje);
                        viajesView.render();
                    } else {
                        toastr.error("Ha ocurrido un error al seleccionar el viaje", 'Error', { positionClass: 'toast-bottom-right' });
                    }
                });

                request.fail(function (res) {
                    toastr.error("Ha ocurrido un error con el servicio", 'Error', { positionClass: 'toast-bottom-right' });
                });

            } catch (error) {
                console.error(error);
            }


        },

        SetCurrentViajero: function (viajero) {
            model.currentViajero = viajero;
        },
        getCurrentViajero: function () {
            return model.currentViajero;
        },
        SetCurrentViaje: function (viaje) {
            model.currentViaje = viaje;
        },
        getCurrentViaje: function () {
            return model.currentViaje;
        }
    };


    var viajeroView = {

        init: function () {
            // Formulario Viajero
            this.viajeroId = $('#ViajeroId');
            this.viajerosSelect = $('#viajerosSelect');
            this.nombre = $('#nombre');
            this.telefono = $('#telefono');
            this.cedula = $('#cedula');

            this.viajerosSelect.change(async () => {
                controller.LoadCurrectViajero(this.viajerosSelect.val());
            });

            //Iniciamos el formulario con el primer valor
            controller.LoadCurrectViajero(this.viajerosSelect.val());

            this.render();
        },

        render: function () {
            var currentViajero = controller.getCurrentViajero();

            this.nombre.val(currentViajero.nombre);
            this.telefono.val(currentViajero.telefono);
            this.cedula.val(currentViajero.cedula);
            this.viajeroId.val(currentViajero.id);
        }

    };


    var viajesView = {
        init: function () {
            this.viajeId = $('#ViajeId');
            this.viajesSelect = $('#viajesSelect');
            this.codigo = $('#codigo');
            this.plazas = $('#plazas');
            this.origen = $('#origen');
            this.destino = $('#destino');
            this.precio = $('#precio');

            this.viajesSelect.on('change', () => {
                controller.LoadCurrentViaje(this.viajesSelect.val());
            });

            controller.LoadCurrentViaje(this.viajesSelect.val());
        },

        render: function () {
            var currentViaje = controller.getCurrentViaje();

            this.viajeId.val(currentViaje.id);

            this.codigo.val(currentViaje.codigo);
            this.plazas.val(currentViaje.plazas);
            this.origen.val(currentViaje.origen);
            this.destino.val(currentViaje.destino);
            this.precio.val(currentViaje.precio);
        }
    };


    //Iniciar controlador de la pagina
    controller.init();

});

