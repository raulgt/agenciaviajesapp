﻿using AgenciaViajesApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;


namespace AgenciaViajesApp.Services
{
    public class ViajesService
    {

        string baseUrl = "https://localhost:44322/";


        public ViajesService() { }

        // Methods

        public async Task<List<Viajes>> ListaViajesAsync()
        {
            List<Viajes> viajes = new List<Viajes>();
            string path = "api/Viajes";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(path);

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var resultParse = JObject.Parse(result)["result"];
                    viajes = JsonConvert.DeserializeObject<List<Viajes>>(resultParse.ToString());

                    return viajes;
                }
            }

            return null;
        }

        public async Task<bool> CrearViajesAsync(Viajes viajes)
        {
            string path = "api/Viajes";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsJsonAsync(path, viajes);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }



        public async Task<Viajes> GetViajeAsync(int id)
        {
            Viajes viaje = new Viajes();
            string path = String.Format("api/Viajes/{0}", id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(path);

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var resultParse = JObject.Parse(result)["result"];
                    viaje = JsonConvert.DeserializeObject<Viajes>(resultParse.ToString());

                    return viaje;
                }
            }

            return null;
        }




        public async Task<bool> UpdateViajeAsync(Viajes viajes)
        {
            string path = "api/Viajes";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PutAsJsonAsync(path, viajes);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }



        public async Task<bool> DeleteViajeAsync(int id)
        {
            string path = String.Format("api/Viajes/{0}", id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.DeleteAsync(path);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }
        

    }
}