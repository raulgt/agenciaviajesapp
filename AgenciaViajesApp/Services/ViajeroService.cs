﻿using AgenciaViajesApp.Models;
using AgenciaViajesApp.Services.ServicesInterfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace AgenciaViajesApp.Services
{
    public  class ViajeroService : IViajeroService
    {
        string baseUrl = "https://localhost:44322/";       
       

        public ViajeroService(){}

        // Methods

        public async Task<List<Viajeros>> ListaViajerosAsync()
        {
            List<Viajeros> viajeros = new List<Viajeros>();
            string path = "api/Viajero";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(path);

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;

                    var resParse = JObject.Parse(result)["result"];

                    viajeros = JsonConvert.DeserializeObject<List<Viajeros>>(resParse.ToString());                    

                    return viajeros;
                }
            }           

            return null;
        }

        public async Task<bool> CrearViajeroAsync(Viajeros viajeros)
        {
            string path = "api/Viajero";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsJsonAsync(path, viajeros);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }

        public async Task<Viajeros> GetViajeroAsync(int id)
        {
            Viajeros viajero = new Viajeros();
            string path = String.Format("api/Viajero/{0}/GetViajero", id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(path);

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var resultParse = JObject.Parse(result)["result"];
                    viajero = JsonConvert.DeserializeObject<Viajeros>(resultParse.ToString());

                    return viajero;
                }
            }

            return null;
        }

        public async Task<bool> UpdateViajeroAsync(Viajeros viajero)
        {
            string path = "api/Viajero";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PutAsJsonAsync(path, viajero);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }

        public async Task<bool> DeleteViajeroAsync(int id)
        {
            string path = String.Format("api/Viajero/{0}", id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.DeleteAsync(path);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }


    }
}