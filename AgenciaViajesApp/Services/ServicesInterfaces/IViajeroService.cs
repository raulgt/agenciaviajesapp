﻿using AgenciaViajesApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgenciaViajesApp.Services.ServicesInterfaces
{
    public interface IViajeroService
    {
      
        Task<List<Viajeros>> ListaViajerosAsync();

        Task<bool> CrearViajeroAsync(Viajeros viajeros);

        Task<Viajeros> GetViajeroAsync(int id);

        Task<bool> UpdateViajeroAsync(Viajeros viajero);

        Task<bool> DeleteViajeroAsync(int id);
    }
}
