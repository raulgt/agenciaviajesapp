﻿using AgenciaViajesApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace AgenciaViajesApp.Services
{
    public class ViajesRealizadosService
    {
        string baseUrl = "https://localhost:44322/";

        public ViajesRealizadosService(){ }

        public async Task<List<ViajesCompletos>> ListaViajesRealizadosAsync()
        {
            List<ViajesCompletos> viajes = new List<ViajesCompletos>();
            string path = "api/ViajesRealizados";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(path);

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;

                    var resultParse = JObject.Parse(result)["result"];

                    viajes = JsonConvert.DeserializeObject<List<ViajesCompletos>>(resultParse.ToString());

                    return viajes;
                }
            }

            return null;
        }

        public async Task<bool> CrearViajeRealizadoAsync(ViajesRealizados viajeRealizado)
        {
            string path = "api/ViajesRealizados";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsJsonAsync(path, viajeRealizado);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }

        public async Task<ViajesRealizados> GetViajeRelizadoPorIdAsync(int id)
        {
            ViajesRealizados viajesRealizados = new ViajesRealizados();
            string path = String.Format("api/ViajesRealizados/{0}/GetById", id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(path);

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var resultParse = JObject.Parse(result)["result"];
                    viajesRealizados = JsonConvert.DeserializeObject<ViajesRealizados>(resultParse.ToString());

                    return viajesRealizados;
                }
            }

            return null;
        }


        public async Task<bool> DeleteViajeRealizadoAsync(int id)
        {
            string path = String.Format("api/ViajesRealizados/{0}", id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.DeleteAsync(path);
                return (response.IsSuccessStatusCode) ? true : false;
            }
        }

    }
}